<?php
require_once __DIR__ ."/../../helper/init.php";
$page_title = "Quick ERP | Manage Customers";
$sidebarSection = 'customer';
$sidebarSubSection = 'manage';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php require_once __DIR__ . "/../includes/head-section.php"; ?>
    <link rel="stylesheet" href="<?= BASEASSETS;?>css/plugins/toastr/toastr.min.css">

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__ . "/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Top Navigation Bar -->
            <?php require_once __DIR__ . "/../includes/navbar.php"; ?>
            <!-- End of Top Navigation Bar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">Manage Customer</h1>
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-8 font-weight-bold text-primary">Customers</h6>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-responsive" id="manage-customer-table" width="100%">
                        <div id="export-buttons"></div>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>GST No</th>
                                    <th>Phone No</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php require_once __DIR__ . "/../includes/footer.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<?php require_once __DIR__ . "/../includes/scroll-to-top.php"; ?>

<?php require_once __DIR__ . "/../includes/core-scripts.php"; ?>
<?php require_once __DIR__ . "/../includes/page-level/customer/manage-customer-scripts.php"; ?>

</body>

</html>
