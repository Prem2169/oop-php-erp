<?php
require_once __DIR__ ."/../../helper/init.php";
$page_title = "Quick ERP | Add New Customer";
$sidebarSection = 'customer';
$sidebarSubSection = 'add';

Util::createCSRFToken();
$errors = "";
$old = "";
if(Session::hasSession('old'))
{
    $old = Session::getSession('old');
    Session::unsetSession('old');
}
if(Session::hasSession('errors'))
{
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php require_once __DIR__ . "/../includes/head-section.php"; ?>

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__ . "/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Top Navigation Bar -->
            <?php require_once __DIR__ . "/../includes/navbar.php"; ?>
            <!-- End of Top Navigation Bar -->

            <!-- Begin Page Content -->
            <!-- container-fluid -->
            <div class="container-fluid">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-4 text-gray-800">Add Customer</h1>
                    <a href="" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                        <i class="fas fa-list-ul fa-sm-text-white"></i>    Manage Customer
                    </a>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card show mb-4">

                                <!-- CARD HEADER -->
                                <div class="card-header">
                                    <h6 class="m-0 font-weight-bold text-primary">
                                        <i class="fa fa-plus"></i>Add Customer
                                    </h6>
                                </div>
                                <!-- CARD HEADER -->

                                <!-- CARD BODY -->
                                <div class="card-body">
                                    <form action="<?= BASEURL;?>helper/routing.php" method="POST" id="add-customer">
                                        <input type="hidden"
                                                name = "csrf_token"
                                                value="<?= Session::getSession('csrf_token');?>">
                                        <!--! FIRST_NAME INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="first_name">First Name</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('first_name') ? 'error' : '') : '';?>"
                                                                name="first_name"
                                                                id="id"
                                                                placeholder="Enter First Name "
                                                                value="<?= $old != '' ? $old['first_name'] : '';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('first_name')):
                                                    echo "<span class='error'> {$errors->first('first_name')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!--! LAST_NAME INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="last_name">Last Name</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('last_name') ? 'error' : '') : '';?>"
                                                                name="last_name"
                                                                placeholder="Enter Last Name "
                                                                value="<?= $old != '' ? $old['last_name'] : '';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('last_name')):
                                                    echo "<span class='error'> {$errors->first('last_name')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!--! GST_NO INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="gst_no">GST NO</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('gst_no') ? 'error' : '') : '';?>"
                                                                name="gst_no"
                                                                placeholder="Enter GST NO "
                                                                value="<?= $old != '' ? $old['gst_no'] : '';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('gst_no')):
                                                    echo "<span class='error'> {$errors->first('gst_no')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!--! PHONE_NO INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="phone_no">Phone Number</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('phone_no') ? 'error' : '') : '';?>"
                                                                name="phone_no"
                                                                placeholder="Enter Phone Number "
                                                                value="<?= $old != '' ? $old['phone_no'] : '';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('phone_no')):
                                                    echo "<span class='error'> {$errors->first('phone_no')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!--! EMAIL_ID INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="email_id">Email</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('email_id') ? 'error' : '') : '';?>"
                                                                name="email_id"
                                                                placeholder="Enter Email "
                                                                value="<?= $old != '' ? $old['email_id'] : '';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('email_id')):
                                                    echo "<span class='error'> {$errors->first('email_id')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!--! GENDER INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="gender">Gender</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('gender') ? 'error' : '') : '';?>"
                                                                name="gender"
                                                                placeholder="Enter Gender "
                                                                value="<?= $old != '' ? $old['gender'] : '';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('gender')):
                                                    echo "<span class='error'> {$errors->first('gender')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div> 

                                        <?php
                                                $currentDate = date('Y-m-d');
                                        ?>
                                        <!--! CREATED_AT INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                
                                                        <input type="hidden"
                                                                name="created_at"
                                                                value="<?=$currentDate;?>"
                                                        >
                                                </div>
                                            </div>
                                        </div> 
                                        <!--! UPDATED_AT INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                
                                                        <input type="hidden"
                                                                name="updated_at"
                                                                value="<?=$currentDate;?>"
                                                        >
                                                </div>
                                            </div>
                                        </div> 

                                        <input type="submit" class="btn btn-primary" name="add_customer" value="Submit">
                                    </form>
                                </div>
                                <!-- /CARD BODY -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php require_once __DIR__ . "/../includes/footer.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<?php require_once __DIR__ . "/../includes/scroll-to-top.php"; ?>

<?php require_once __DIR__ . "/../includes/core-scripts.php"; ?>
<script src="<?=BASEASSETS;?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?=BASEASSETS;?>js/pages/customer/add-customer.js"></script>

</body>

</html>
