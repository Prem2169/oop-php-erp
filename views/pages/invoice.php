<?php
require_once __DIR__ ."/../../helper/init.php";
$result = "";
$particulars = "";
$finalTotal = 0;
if(isset($_GET['id']))
{
    // Util::dd($_GET['id']);
    $result = $di->get('invoice')->getSalesProductsByInvoiceid($_GET['id']);
    $particulars = $result['particulars'];
    // Util::dd($particulars);
    
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Invoice</title>
    <?php require_once __DIR__ . "/../includes/head-section.php"; ?>
</head>
<body>

<div id="wrapper" class="">

<!-- Sidebar -->
<?php require_once __DIR__ . "/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column ">

    <!-- Main Content -->
        <div id="content">
            <div class="container-fluid pt-2">
                <div class="container-fluid">
                    <div class="d-sm-flex align-items-center justify-content-between">
                        <h1 class="h3 mb-2 text-gray-800">Invoice</h1>
                    </div>
                    <!-- CUSTOMER -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card show mb-1">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1 class="h3 text-gray-800">Customer</h1>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Name</label>
                                                <input type="text" class="form-control" readonly value="<?=$result['customer'][0]->first_name . " " . $result['customer'][0]->last_name?>">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Phone No</label>
                                                <input type="text" class="form-control" readonly value="<?=$result['customer'][0]->phone_no?>">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Email Id</label>
                                                <input type="text" class="form-control" readonly value="<?=$result['customer'][0]->email_id?>">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">GST No</label>
                                                <input type="text" class="form-control" readonly value="<?=$result['customer'][0]->gst_no?>">
                                            </div>
                                        </div>
                                    </div>        
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CUSTOMER -->
                </div>
                <div class="container-fluid ">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="card show mb-2">
                                <div class="col-md-12 mt-4">
                                    <h1 class="h3 ml-2 text-gray-800">Particulars</h1>
                                </div>
                                <div class="card-body pt-0">
                                    <div id="products_container">
                                    <?php
                                    foreach($particulars as $particular)
                                    {
                                        // Util::dd($particular);
                                        $finalTotal += $particular->final_rate;
                                    ?>
                                        <!-- BEGIN: PRODUCT CUSTOM CONTROL -->
                                        <div class="row product_row">
                                            <!-- BEGIN: PRODUCT SELECT -->
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="">Products</label>
                                                    <input type="text" class="form-control" readonly value="<?=$particular->product_name?>">
                                                </div>
                                            </div>
                                            <!-- BEGIN: PRODUCT -->
                                            <!-- BEGIN: SELLING PRICE -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Selling price</label>
                                                    <input type="text" class="form-control" value="<?=$particular->selling_rate?>" readonly>
                                                </div>
                                            </div>
                                            <!-- BEGIN: SELLING PRICE -->
                                            <!-- BEGIN: QUANTITY -->
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <label for="">Quantity</label>
                                                    <input type="number" class="form-control quantity_changed" value="<?=$particular->quantity?>" disabled>
                                                </div>
                                            </div>
                                            <!-- BEGIN: QUANTITY -->
                                            <!-- BEGIN: DISCOUNT -->
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <label for="">Discount</label>
                                                    <input type="number" class="form-control discount_changed" value="<?=$particular->discount?>" disabled>
                                                </div>
                                            </div>
                                            <!-- BEGIN: DISCOUNT -->
                                            <!-- BEGIN: FINAL_RATE -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Final Rate</label>
                                                    <input type="text"  class="form-control final_rate" value="<?=$particular->final_rate?>" readonly>
                                                </div>
                                            </div>
                                            <!-- BEGIN: FINAL_RATE -->
                                        </div>
                                        <!-- BEGIN: PRODUCT CUSTOM CONTROL -->
                                    <?php
                                    }
                                    ?>
                                    </div>
                                </div>
                                <!-- BEGIN: CARD FOOTER-->
                                <div class="card-footer d-flex justify-content-end">
                                    <div class="form-group row">
                                        <label for="finalTotal" class="col-sm-4 col-form-label">Final Total</label>
                                        <div class="col-sm-8">
                                            <input type="text" readonly class="form-control" id="finalTotal" value="<?=$finalTotal?>">
                                        </div>
                                    </div>
                                </div>
                                <!-- END: CARD FOOTER-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <?php require_once __DIR__ . "/../includes/core-scripts.php"; ?>
<script src="<?=BASEASSETS;?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?=BASEASSETS;?>js/pages/transactions/add-sales.js"></script>

</body>

</html>