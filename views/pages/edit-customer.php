<?php
require_once __DIR__ ."/../../helper/init.php";
$page_title = "Quick ERP | Edit New Customer";
$sidebarSection = 'customer';
$sidebarSubSection = 'edit';

Util::createCSRFToken();
$errors = "";
$old = "";
if(Session::hasSession('old'))
{
    $old = Session::getSession('old');
    Session::unsetSession('old');
    // Util::dd($old);
    echo "Old : ";
    var_dump($old);
}
if(Session::hasSession('errors'))
{
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
    // echo "Errors : ";
    var_dump($errors);
    // Util::dd($errors);
}

//The below code will run when someone likes to edit the customer from manage-customer.php (that pencil edit button)
$editCustomer = "";

if(isset($_POST)){
    if(Session::hasSession('edit-customer')){
        $editCustomer = Session::getSession('edit-customer');
        Session::unsetSession('edit-customer');
        // Util::dd($editCustomer);
    }else{
        Session::setSession('edit-customer', $_POST);
        // $editCustomer = Session::getSession('edit-customer');
        var_dump($_POST);
    }
}
// var_dump($editCustomer);
// echo "edit customer : ";
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php require_once __DIR__ . "/../includes/head-section.php"; ?>

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__ . "/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Top Navigation Bar -->
            <?php require_once __DIR__ . "/../includes/navbar.php"; ?>
            <!-- End of Top Navigation Bar -->

            <!-- Begin Page Content -->
            <!-- container-fluid -->
            <div class="container-fluid">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-4 text-gray-800">Edit Customer</h1>
                    <a href="" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                        <i class="fas fa-list-ul fa-sm-text-white"></i>    Manage Customer
                    </a>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card show mb-4">

                                <!-- CARD HEADER -->
                                <div class="card-header">
                                    <h6 class="m-0 font-weight-bold text-primary">
                                        <i class="fa fa-plus"></i>Edit Customer
                                    </h6>
                                </div>
                                <!-- CARD HEADER -->

                                <!-- CARD BODY -->
                                <div class="card-body">
                                    <form action="<?= BASEURL;?>helper/routing.php" method="POST" id="edit-customer">
                                        <input type="hidden"
                                                name = "csrf_token"
                                                value="<?= Session::getSession('csrf_token');?>">
                                        <!-- 
                                            here in the value attribute of this below input there are two array one is $old and other is $editCustomer
                                            so the point is i have used here for $old : $old['customer_id'] and for $editCustomer : $editCustomer['id']
                                            here both the parameter inside the $old and $editCustomer are different bcoz the thing is : 
                                            $old is the array which is send by this form only means when we click the submit or saveChanges of this form and then at that time if some type of error occurs then this whole data is stored inside the $old array so idr ye input ka 'name' attribute 'customer_id' so isleye humko jbi $old m id use krni h toh hum $old['customer_id'] likha h itna samja ?!!
                                            Now other is $editCustomer so editCustomer array m data tbi jayegii jbu hum manage-customer.php m se koi data pe edit click krenge toh tbi kya krta h vo us data jispe humne click kia h so vo data database m lekr aata h and idr store krta h means editCustomer m store krta h and inside db we have used 'id' column for storing id so isleye jbi humko editCustomer se 'id' use krna h toh humne $editCustomer['id'] likha tha abi clear!! :) 
                                         -->
                                        <input type="hidden" 
                                        name="customer_id" 
                                        id="edit_customer_id"
                                        value="<?= $old != '' ? $old['customer_id'] : $editCustomer !='' ? isset($editCustomer['id']) ? $editCustomer['id'] : $old['customer_id'] : 'not found name';?>">
                                        <!--! FIRST_NAME INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="first_name">First Name</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('first_name') ? 'error' : '') : '';?>"
                                                                name="first_name"
                                                                id="id"
                                                                placeholder="Enter First Name "
                                                                value="<?= $old != '' ? $old['first_name'] : $editCustomer !='' ? isset($editCustomer['first_name']) ? $editCustomer['first_name'] : $old['first_name'] : 'not found name';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('first_name')):
                                                    echo "<span class='error'> {$errors->first('first_name')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!--! LAST_NAME INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="last_name">Last Name</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('last_name') ? 'error' : '') : '';?>"
                                                                name="last_name"
                                                                placeholder="Enter Last Name "
                                                                value="<?= $old != '' ? $old['last_name'] : $editCustomer !='' ? isset($editCustomer['last_name']) ? $editCustomer['last_name'] : $old['last_name'] : 'not found name';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('last_name')):
                                                    echo "<span class='error'> {$errors->first('last_name')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!--! GST_NO INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="gst_no">GST NO</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('gst_no') ? 'error' : '') : '';?>"
                                                                name="gst_no"
                                                                placeholder="Enter GST NO "
                                                                value="<?= $old != '' ? $old['gst_no'] : $editCustomer !='' ? isset($editCustomer['gst_no']) ? $editCustomer['gst_no'] : $old['gst_no'] : 'not found name';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('gst_no')):
                                                    echo "<span class='error'> {$errors->first('gst_no')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!--! PHONE_NO INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="phone_no">Phone Number</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('phone_no') ? 'error' : '') : '';?>"
                                                                name="phone_no"
                                                                placeholder="Enter Phone Number "
                                                                value="<?= $old != '' ? $old['phone_no'] : $editCustomer !='' ? isset($editCustomer['phone_no']) ? $editCustomer['phone_no'] : $old['phone_no'] : 'not found name';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('phone_no')):
                                                    echo "<span class='error'> {$errors->first('phone_no')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!--! EMAIL_ID INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="email_id">Email</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('email_id') ? 'error' : '') : '';?>"
                                                                name="email_id"
                                                                placeholder="Enter Email "
                                                                value="<?= $old != '' ? $old['email_id'] : $editCustomer !='' ? isset($editCustomer['email_id']) ? $editCustomer['email_id'] : $old['email_id'] : 'not found name';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('email_id')):
                                                    echo "<span class='error'> {$errors->first('email_id')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!--! GENDER INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="gender">Gender</label>
                                                        <input type="text"
                                                                class="form-control <?= $errors!='' ? ($errors->has('gender') ? 'error' : '') : '';?>"
                                                                name="gender"
                                                                placeholder="Enter Gender "
                                                                value="<?= $old != '' ? $old['gender'] : $editCustomer !='' ? isset($editCustomer['gender']) ? $editCustomer['gender'] : $old['gender'] : 'not found name';?>"
                                                        >
                                                        <?php
                                                if($errors!="" && $errors->has('gender')):
                                                    echo "<span class='error'> {$errors->first('gender')}</span>";
                                                endif;
                                                ?>
                                                </div>
                                            </div>
                                        </div> 

                                        <?php
                                                $currentDate = date('Y-m-d');
                                        ?>
                                        <!--! CREATED_AT INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                
                                                        <input type="hidden"
                                                                name="created_at"
                                                                value="<?= $old != '' ? $old['created_at'] : $editCustomer !='' ? isset($editCustomer['created_at']) ? $editCustomer['created_at'] : $old['created_at'] : 'not found created_at';?>"
                                                        >
                                                </div>
                                            </div>
                                        </div> 
                                        <!--! UPDATED_AT INPUT -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                
                                                        <input type="hidden"
                                                                name="updated_at"
                                                                value="<?=$currentDate;?>"
                                                        >
                                                </div>
                                            </div>
                                        </div> 

                                        <input type="submit" class="btn btn-primary" name="edit_customer" value="Save Changes">
                                        <input type="submit" class="btn btn-primary btn-secondary" name="edit_cancel_customer" value="Cancel">
                                    </form>
                                </div>
                                <!-- /CARD BODY -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php require_once __DIR__ . "/../includes/footer.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<?php require_once __DIR__ . "/../includes/scroll-to-top.php"; ?>

<?php require_once __DIR__ . "/../includes/core-scripts.php"; ?>
<script src="<?=BASEASSETS;?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?=BASEASSETS;?>js/pages/customer/add-customer.js"></script>

</body>

</html>
