<?php
require_once 'init.php';
// Util::dd($_POST);

if(isset($_POST['add_category']))
{
    if(Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('category')->addCategory($_POST);
        // Util::dd($result);

        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'Add Category Error!');
                Util::redirect("manage-category.php");
            break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'Add Category Success!');
                Util::redirect("manage-category.php");
            break;
            case VALIDATION_ERROR:
                Session::setSession('validation', "Validation Error");
                Session::setSession('old', $_POST);
                Session::setSession('errors', serialize($di->get('category')->getValidator()->errors()));
                Util::redirect("add-category.php");
            break;
        }
    }
    else
    {
        Session::setSession("csrf", 'CSRF ERROR');
        Util::redirect("manage-category.php");//need to change this, actually we will redirecting to some error page indicating unauthorized access 
    }
}

if(isset($_POST['add_customer']))
{
    if(Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('customer')->addCustomer($_POST);
        // Util::dd($result);

        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'Add Customer Error!');
                Util::redirect("manage-customer.php");
            break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'Add Customer Success!');
                Util::redirect("manage-customer.php");
            break;
            case VALIDATION_ERROR:
                Session::setSession('validation', "Validation Error");
                Session::setSession('old', $_POST);
                Session::setSession('errors', serialize($di->get('customer')->getValidator()->errors()));
                Util::redirect("add-customer.php");
            break;
        }
    }
    else
    {
        Session::setSession("csrf", 'CSRF ERROR');
        Util::redirect("manage-customer.php");//need to change this, actually we will redirecting to some error page indicating unauthorized access 
    }
}

if(isset($_POST['add_product']))
{
    if(Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('product')->addProduct($_POST);
        // Util::dd($result);

        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'Add Product Error!');
                Util::redirect("manage-product.php");
            break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'Add Product Success!');
                Util::redirect("manage-product.php");
            break;
            case VALIDATION_ERROR:
                Session::setSession('validation', "Validation Error");
                Session::setSession('old', $_POST);
                Session::setSession('errors', serialize($di->get('product')->getValidator()->errors()));
                Util::redirect("add-product.php");
            break;
        }
    }
    else
    {
        Session::setSession("csrf", 'CSRF ERROR');
        Util::redirect("manage-product.php");//need to change this, actually we will redirecting to some error page indicating unauthorized access 
    }
}

if(isset($_POST['page']))
{
    // $_POST['search']['value']
    // $_POST['start']
    // $_POST['length']
    // $_POST['draw']
    if($_POST['page'] == 'manage_category')
    {
        // Util::dd($_POST);
        $search_parameter = $_POST['search']['value'] ?? null;
        $order_by = $_POST['order'] ?? null;
        $start = $_POST['start'];
        $length = $_POST['length'];
        $draw = $_POST['draw'];
        $di->get('category')->getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length);
    }
    if($_POST['page'] == 'manage_customer')
    {
        $search_parameter = $_POST['search']['value'] ?? null;
        $order_by = $_POST['order'] ?? null;
        $start = $_POST['start'];
        $length = $_POST['length'];
        $draw = $_POST['draw'];
        $di->get('customer')->getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length);
    }
    if($_POST['page'] == 'manage_product')
    {
        // Util::dd($_POST);
        $search_parameter = $_POST['search']['value'] ?? null;
        $order_by = $_POST['order'] ?? null;
        $start = $_POST['start'];
        $length = $_POST['length'];
        $draw = $_POST['draw'];
        $di->get('product')->getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length);
    }
}

if(isset($_POST['fetch']))
{
    if($_POST['fetch'] == 'category')
    {
        $category_id = $_POST['category_id'];
        $result = $di->get('category')->getCategoryByID($category_id, PDO::FETCH_ASSOC);
        echo json_encode($result[0]);
    }
    if($_POST['fetch'] == 'customer')
    {
        $customer_id = $_POST['customer_id'];
        $result = $di->get('customer')->getCustomerByID($customer_id, PDO::FETCH_ASSOC);
        // Util::dd($result[0]);
        echo json_encode($result[0]);
    }

}

if(isset($_POST['editCategory']))
{
    if(Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('category')->update($_POST, $_POST['category_id']);
        switch($result)
        {
            case UPDATE_ERROR:
                Session::setSession(UPDATE_ERROR, 'Update Category Error!');
                Util::redirect("manage-category.php");
            break;
            case UPDATE_SUCCESS:
                Session::setSession(UPDATE_SUCCESS, 'Update Category Success!');
                Util::redirect("manage-category.php");
            break;
            case VALIDATION_ERROR:
                Session::setSession('validation', "Validation Error");
                Session::setSession('old', $_POST);
                Session::setSession('errors', serialize($di->get('category')->getValidator()->errors()));
                Util::redirect("manage-category.php");
            break;
        }
    }
    else
    {
        Session.setSession("csrf", "CSRF ERROR");
        Util::redirect("manage-category.php");
        //Unauthorized access
    }
}

if(isset($_POST['deleteCategory']))
{
    if(Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('category')->delete($_POST['record_id']);
        switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, 'Delete Category Error!');
                Util::redirect("manage-category.php");
            break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, 'Delete Category Success!');
                Util::redirect("manage-category.php");
            break;
        }
    }
    else
    {
        Session.setSession("csrf", "CSRF ERROR");
        Util::redirect("manage-category.php");
        //Unauthorized access
    }
}

if(isset($_POST['edit_customer']))
{
    // Util::dd($_POST);
    if(Util::verifyCSRFToken($_POST))
    {
        // Util::dd($_POST['customer_id']);
        $result = $di->get('customer')->update($_POST, $_POST['customer_id']);
        // Util::dd($result);

        switch($result)
        {
            case UPDATE_ERROR:
                Session::setSession(UPDATE_ERROR, 'Update Customer Error!');
                Util::redirect("manage-customer.php");
            break;
            case UPDATE_SUCCESS:
                // Util::dd("Successs");
                Session::setSession(UPDATE_SUCCESS, 'Update Customer Success!');
                Util::redirect("manage-customer.php");
            break;
            case VALIDATION_ERROR:
                Session::setSession('validation', "Validation Error");
                Session::setSession('old', $_POST);
                Session::setSession('errors', serialize($di->get('customer')->getValidator()->errors()));
                Util::redirect("edit-customer.php");
            break;
        }
    }
    else
    {
        Session::setSession("csrf", 'CSRF ERROR');
        Util::redirect("manage-customer.php");//need to change this, actually we will redirecting to some error page indicating unauthorized access 
    }
}
if(isset($_POST['edit_cancel_customer']))
{
    Util::redirect("manage-customer.php");
}

if(isset($_POST['getCategories']))
{
    echo JSON_encode($di->get('category')->all());
}
if(isset($_POST['getProductsByCategoryID']))
{
    $category_id = $_POST['categoryID'];
    $result = $di->get('product')->getProductsByCategoryId($category_id);
    echo JSON_encode($result);
}
if(isset($_POST['getProductSellingPrice']))
{
    $product_id = $_POST['productID'];
    $result = $di->get('product')->getProductSellingPrice($product_id);
    // Util::dd($result);
    echo JSON_encode($result);
}
if(isset($_POST['getCustomerByEmail']))
{
    $email = $_POST['customerEmail'];
    $result = $di->get('customer')->getCustomerByEmail($email);
    echo JSON_encode($result);
}
if(isset($_POST['add_sales']))
{
    if($_POST['customer_id'] != 0 && count($_POST['product_id']) >= 1)
    {
        $customer_id = $_POST['customer_id'];
        $products_id = $_POST['product_id'];//multiple products id will be there minimum 1
        $quantities = $_POST['quantity'];//multiple quatity will be there minimum 1
        $discounts = $_POST['discount'];//multiple discount will be there minimum 1
        $res = $di->get('sale')->create($customer_id, $products_id, $quantities,$discounts);
        // Util::dd($res);
        Util::redirect("invoice.php?id={$res}");
    }
    else{
        echo "No customer Found ==== ";
        echo "Go back\n";
        echo "Will change this after";
        //this is for testing purpose will change this
    }
}