<?php
ini_set('display_errors', 'on');
error_reporting(E_ALL);

session_start();

require_once __DIR__ . "/requirements.php";

$di = new DependencyInjector();
$di->set('config', new Config());
$di->set('database', new Database($di));
$di->set('hash', new Hash());
$di->set('util', new Util($di));
$di->set('error_handler', new ErrorHandler());
$di->set('validator', new Validator($di));
$di->set('category', new Category($di));
$di->set('customer', new Customer($di));
$di->set('product', new Product($di));
$di->set('invoice', new Invoice($di));
$di->set('sale', new Sale($di));

require_once __DIR__ . "/constants.php";

// Util::createCSRFToken();

// SELECT t1.product_id, t1.selling_rate, t1.with_effect_from FROM products_selling_rate t1 INNER JOIN (SELECT products_selling_rate.product_id, products_selling_rate.selling_rate, MAX(products_selling_rate.with_effect_from) as wef FROM products_selling_rate INNER JOIN sales ON products_selling_rate.product_id = sales.product_id and products_selling_rate.with_effect_from <= sales.created_at and sales.invoice_id = 2 ) as t2 ON t2.product_id = t1.product_id and t2.wef = t1.with_effect_from

//INVOICE GENERAL HALF
// SELECT invoice.customer_id, t1.product_id, products.name as product_name, t1.selling_rate, t1.with_effect_from,sales.id as sale_id, sales.quantity, sales.discount FROM invoice INNER JOIN sales ON sales.invoice_id=invoice.id INNER JOIN products_selling_rate t1 INNER JOIN (SELECT products_selling_rate.id, products_selling_rate.product_id, products_selling_rate.selling_rate, products_selling_rate.with_effect_from as wef, sales.id as sale_id FROM sales INNER JOIN products_selling_rate ON products_selling_rate.product_id=sales.product_id AND products_selling_rate.with_effect_from <= sales.created_at AND sales.invoice_id = 4 ORDER BY products_selling_rate.id DESC LIMIT 3) as t2 ON t2.product_id = t1.product_id and t2.wef = t1.with_effect_from INNER JOIN products ON sales.product_id = products.id WHERE sales.invoice_id = 4 AND t1.product_id = sales.product_id GROUP BY product_id