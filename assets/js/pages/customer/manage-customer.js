// import  {post}  from  '/assets/vendor/jquery/jquery.min.js';
{/* <script src="//code.jquery.com/jquery-1.11.0.min.js"></script> */}

var TableDatatables = function(){
    var handleCustomerTable = function(){
        var manageCustomerTable = $("#manage-customer-table");
        var baseURL = window.location.origin;
        var filePath = "/helper/routing.php";
        var oTable = manageCustomerTable.DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + filePath,
                type: "POST",
                data: {
                    "page": "manage_customer"
                }
            },
            "lengthMenu" : [
                [5, 15, 25, -1],
                [5, 15, 25, "All"]
            ],
            "order" : [
                // [1, "desc"]
            ],
            "columnDefs": [
                {
                    'orderable': false,
                    'targets': [0, 3, 4, 5, 6]
                }
            ]
        });
        manageCustomerTable.on('click', '.edit', function(e){
            var id = $(this).data('id');
            //Fetching all other values from the database using AJAX and loading them onto their respective fields in the modal.
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "customer_id": id,
                    "fetch": "customer"
                },
                dataType: "json",
                success: async function(data){
                    console.log(data);
                    $.post(baseURL + '/views/pages/edit-customer.php', data, function(data){
                        // console.log(data);
                        window.location = baseURL + '/views/pages/edit-customer.php';
                    }
                    );
                    
                }
            })
        });
        new $.fn.dataTable.Buttons( oTable, {
            buttons: [
                'copy', 'csv','pdf'
            ]
        });
        oTable.buttons().container().appendTo($('#export-buttons'));
    }
    return {
        //main function to handle all the datatables
        init: function() {
            handleCustomerTable();
        }
    }
}();
$(document).ready(function(){
    TableDatatables.init();
});