var id = 2;
var baseUrl = window.location.origin;
var filePath = "/helper/routing.php";
function clearFields(element_id){
    $("#selling_price_" + element_id).val(0);
    $("#quantity_" + element_id).val(1);
    $("#discount_" + element_id).val(0);
    $("#final_rate_" + element_id).val(0);
}
function calculateFinalTotalPrice(){
    var allFinalRates = $('.final_rate');
    var sum = 0;
    for(let i=0; i<allFinalRates.length; i++){
        sum += parseInt(allFinalRates[i].value);
    }
    $("#finalTotal").val(sum);
}
function addProduct(){
    $("#products_container").append(
        `
        <!-- BEGIN: PRODUCT CUSTOM CONTROL -->
        <div class="row product_row" id="element_${id}">
            <!-- BEGIN: CATEGORY SELECT -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Category</label>
                    <select id="category_${id}" class="form-control category_select">
                        <option disabled selected> Select Category</option>
                    </select>
                </div>
            </div>
            <!-- BEGIN: CATEGORY SELECT -->
            <!-- BEGIN: PRODUCT SELECT -->
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Products</label>
                    <select name="product_id[]" id="product_${id}" class="form-control product_select">
                        <option disabled selected> Select Products</option>
                    </select>
                </div>
            </div>
            <!-- BEGIN: PRODUCT -->
            <!-- BEGIN: SELLING PRICE -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Selling price</label>
                    <input type="text" id="selling_price_${id}" class="form-control" readonly>
                </div>
            </div>
            <!-- BEGIN: SELLING PRICE -->
            <!-- BEGIN: QUANTITY -->
            <div class="col-md-1">
                <div class="form-group">
                    <label for="">Quantity</label>
                    <input type="number" name="quantity[]" id="quantity_${id}" class="form-control quantity_changed" placeholder="Enter Quantity" value="0">
                </div>
            </div>
            <!-- BEGIN: QUANTITY -->
            <!-- BEGIN: DISCOUNT -->
            <div class="col-md-1">
                <div class="form-group">
                    <label for="">Discount</label>
                    <input type="number" name="discount[]" id="discount_${id}" class="form-control discount_changed" placeholder="Enter Discount" value="0">
                </div>
            </div>
            <!-- BEGIN: DISCOUNT -->
            <!-- BEGIN: FINAL_RATE -->
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">Final Rate</label>
                        <input type="text" name="final_rate[]" id="final_rate_${id}" class="form-control final_rate" value="0" readonly>
                    </div>
                </div>
            <!-- BEGIN: FINAL_RATE -->
            <!-- BEGIN: DELETE -->
            <div class="col-md-1">
                <button type="button" class="btn btn-danger mt-4" style="margin-top: 43%!important" onclick="deleteProduct(${id})">
                    <i class="fas fa-trash-alt fa-sm text-white"></i>
                </button>
            </div>
            <!-- BEGIN: DELETE -->
        </div>
        <!-- BEGIN: PRODUCT CUSTOM CONTROL -->
        `
    );
    $.ajax({
        url: baseUrl + filePath,
        method: "POST",
        data: {
            getCategories: true
        },
        dataType: 'json',
        success : function(categories){
            categories.forEach(function(category){
                $("#category_" + id).append(
                    `<option value='${category.id}'>${category.name}</option>`
                );
            });
            id++;
        }
    });
}
function deleteProduct(delete_id){
    var elements = document.getElementsByClassName("product_row");
    if(elements.length != 1)
    {
        $("#element_" + delete_id).remove();
    }
}
$("#products_container").on('change', '.category_select', function(){
    var element_id = $(this).attr('id').split('_')[1];
    var category_id = this.value;
    clearFields(element_id);
    $.ajax({
        url: baseUrl + filePath,
        method: "POST",
        data: {
            getProductsByCategoryID: true,
            categoryID: category_id,
        },
        dataType: 'json',
        success : function(products){
            $("#product_" + element_id).empty();
            $("#product_" + element_id).append(`
                <option disabled selected> Select Products</option>
            `);
            products.forEach(function(product){
                $("#product_" + element_id).append(
                    `<option value='${product.id}'>${product.name}</option>`
                );
            });
        }
    });
});
$("#products_container").on('change', '.product_select', function(){
    var element_id = $(this).attr('id').split('_')[1];
    var product_id = this.value;
    clearFields(element_id);
    $.ajax({
        url: baseUrl + filePath,
        method: "POST",
        data: {
            getProductSellingPrice: true,
            productID: product_id,
        },
        dataType: 'json',
        success : function(data){
            $("#selling_price_" + element_id).val(data[0].selling_rate);
            $("#final_rate_" + element_id).val(data[0].selling_rate);
            calculateFinalTotalPrice();
        }
    });
});
$("#products_container").on('change', '.quantity_changed', function(){
    var element_id = $(this).attr('id').split('_')[1];
    var quantityValue = this.value;
    var sellingPrice = $("#selling_price_" + element_id).val();
    var discountElement = $("#discount_" + element_id);
    var finalRateElement = $("#final_rate_" + element_id);
    if(!(quantityValue <= 0))
    {
        finalRateElement.val(sellingPrice * quantityValue);
    }
    else{
        finalRateElement.val(0);
    }
    if(!(discountElement.val() <= 0))
    {
        finalRateElement.val(finalRateElement.val() - ( finalRateElement.val() * (discountElement.val() / 100) ));
    }
    calculateFinalTotalPrice();

});
$("#products_container").on('change', '.discount_changed', function(){
    var element_id = $(this).attr('id').split('_')[1];
    var quantityValue = $("#quantity_" + element_id).val();;
    var sellingPrice = $("#selling_price_" + element_id).val();
    var discountElement = $("#discount_" + element_id);
    var finalRateElement = $("#final_rate_" + element_id);
    let finalRateValue = 0;
    if(!(quantityValue <= 0))
    {
        finalRateElement.val(sellingPrice * quantityValue);
        finalRateValue = finalRateElement.val();
    }else{
        finalRateElement.val(0);
    }
    if(!(discountElement.val() <= 0))
    {
        finalRateValue = (finalRateValue - ( finalRateValue * (discountElement.val() / 100) ));
        finalRateElement.val(finalRateValue);
    }else{
        finalRateElement.val(0);
    }
    calculateFinalTotalPrice();

});

$('#check_email').on('click', function(){
    var customerEmail = document.getElementById("customer_email").value;
    if(customerEmail != ""){
        $.ajax({
            url: baseUrl + filePath,
            method: "POST",
            data: {
                getCustomerByEmail: true,
                customerEmail: customerEmail,
            },
            dataType: 'json',
            success : function(data){
                var emailSuccess = document.getElementById('email_verify_success');
                var emailFail = document.getElementById('email_verify_fail');
                var addCustomerBtn = document.getElementById('add_customer_btn');
                if(data.length == 1)
                {
                    $('#customer_id').val(data[0].id);
                    if(emailSuccess.classList.contains('d-none')){
                        emailSuccess.classList.toggle('d-none');
                        emailSuccess.classList.toggle('d-inline-block');
                    }
                    if(emailFail.classList.contains('d-inline-block')){
                        emailFail.classList.toggle('d-none');
                        emailFail.classList.toggle('d-inline-block');
                        addCustomerBtn.classList.toggle('d-none');
                        addCustomerBtn.classList.toggle('d-inline-block');
                    }
                    // document.getElementById('addSalesSubmitBtn').style("display : disabled");   
                }
                else{
                    $('#customer_id').val(0);
                    if(emailSuccess.classList.contains('d-inline-block')){
                        emailSuccess.classList.toggle('d-none');
                        emailSuccess.classList.toggle('d-inline-block');
                    }
                    if(emailFail.classList.contains('d-none')){
                        emailFail.classList.toggle('d-none');
                        emailFail.classList.toggle('d-inline-block');
                        addCustomerBtn.classList.toggle('d-none');
                        addCustomerBtn.classList.toggle('d-inline-block');
                    }

                }
            }
        });
    }
}); 
// $('#addSalesSubmitBtn').on('click', function(){
//     console.log(this);
// })