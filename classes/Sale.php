<?php

class Sale
{
    private $table = "sales";
    protected $di;
    private $database;

    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function create($customer_id, $products_id, $quantities,$discounts){
        // Util::dd($discounts);
        $invoiceId = $this->di->get('invoice')->create($customer_id); 
        // Util::dd($invoiceId);
        $lastId = "";
        for($i=0; $i < count($products_id); $i++)
        {
            $data_to_be_inserted = ['invoice_id' => $invoiceId];
            $data_to_be_inserted['discount'] = $discounts[$i];
            $data_to_be_inserted['quantity'] = $quantities[$i];
            $data_to_be_inserted['product_id'] = $products_id[$i];

            $lastId = $this->database->insert($this->table, $data_to_be_inserted);
        }
        return $invoiceId;
    }
    public function getProductsCountByInvoiceId($invoice_id)
    {
        $countQuery = "SELECT COUNT(*) as count FROM sales WHERE sales.invoice_id={$invoice_id}";
        $res = $this->database->raw($countQuery);
        return $res[0]->count;
    }
}