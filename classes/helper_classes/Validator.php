<?php

class Validator
{
    private $di;

    protected $database;
    protected $errorHandler;

    protected $rules = ["required", "minlength", "maxlength", "unique", "email"];
    protected $messages = [
        "required" => "The :field field is required very",
        "minlength" => "The :field field must be a minimum of :satisfier characters",
        "maxlength" => "The :field field must be a maximum of :satisfier characters",
        "email" => "This :field is not valid email address",
        "unique" => "That :field is already taken"
    ];

    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
        $this->errorHandler = $this->di->get('error_handler');
    }
    public function check($items, $rules)
    {
        // var_dump($items);
        // Util::dd($items);
        foreach($items as $item=>$value)
        {
            if(in_array($item, array_keys($rules)))
            {
                $this->validate([
                    'field' => $item,
                    'value' => $value,
                    'rules' => $rules[$item],
                ]);
            }
        }
        return $this;
    }
    private function validate($item){
        // $item should contains = [ 'field', 'value', 'rules']
        
        // die(var_dump($item));
        $field = $item['field'];
        $value = $item['value'];
        foreach($item['rules'] as $rule=>$satisfier)
        {
            if(!call_user_func_array([$this, $rule], [$field, $value, $satisfier]))
            {
                // Util::dd($this);
                //store the error in the error handler
                $this->errorHandler->addError(str_replace([':field', ':satisfier'], [$field, $satisfier], $this->messages[$rule]), $field);
            }
        }
    }
    public function fails()
    {
        return $this->errorHandler->hasErrors();
    }
    public function errors()
    {
        return $this->errorHandler;
    }
    private function required($field, $value, $satisfier)
    {
        return !empty(trim($value));
    }
    private function minlength($field, $value, $satisfier)
    {
        return mb_strlen($value) >= $satisfier;
    }
    private function maxlength($field, $value, $satisfier)
    {
        return mb_strlen($value) <= $satisfier;
    }
    private function unique($field, $value, $satisfier)
    {
        //Here #satisfier will become the name of table!
        //$field will become the name of column
        //$value should be the unique under the above table and column
        return !$this->database->exists($satisfier, [$field=>$value]);
    }
    private function uniqueExceptCurrentId($field, $value, $satisfier)
    {
        //Here #satisfier will become the name of table!
        //$field will become the name of column
        //$value should be the unique under the above table and column

        // Util::dd($satisfier);

        return !$this->database->existsExceptCurrentId($satisfier, [$field=>$value]);
    }
    private function email($field, $value, $satisfier)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }
    
}