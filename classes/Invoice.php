<?php

class Invoice{
    private $table = "invoice";
    protected $di;
    private $database;

    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function create($customer_id){
        // Util::dd($discounts);
        $data_to_be_inserted = ['customer_id'=>$customer_id];
        return $this->database->insert($this->table, $data_to_be_inserted);
    }
    public function getSalesProductsByInvoiceId($invoice_id)
    {
        $products_count = $this->di->get('sale')->getProductsCountByInvoiceId($invoice_id);
        
        $query = "SELECT invoice.customer_id, t1.product_id, products.name as product_name, t1.selling_rate, t1.with_effect_from,sales.id as sale_id, sales.quantity, sales.discount FROM invoice INNER JOIN sales ON sales.invoice_id=invoice.id INNER JOIN products_selling_rate t1 INNER JOIN (SELECT products_selling_rate.id, products_selling_rate.product_id, products_selling_rate.selling_rate, products_selling_rate.with_effect_from as wef, sales.id as sale_id FROM sales INNER JOIN products_selling_rate ON products_selling_rate.product_id=sales.product_id AND products_selling_rate.with_effect_from <= sales.created_at AND sales.invoice_id = {$invoice_id} ORDER BY products_selling_rate.id DESC LIMIT {$products_count}) as t2 ON t2.product_id = t1.product_id and t2.wef = t1.with_effect_from INNER JOIN products ON sales.product_id = products.id WHERE sales.invoice_id = {$invoice_id} AND t1.product_id = sales.product_id GROUP BY product_id";
        $queryResult = $this->database->raw($query);

        $customer = $this->di->get('customer')->getCustomerById($queryResult[0]->customer_id);

        foreach($queryResult as $row)
        {
            $quantity = $row->quantity;
            $discount = $row->discount;
            $selling_rate = $row->selling_rate;
            $quantity_selling = $selling_rate * $quantity;
            $row->final_rate = (int)($quantity_selling - ($quantity_selling * ($discount / 100)));
        }
        // Util::dd($queryResult);
        $data_to_be_sent = ["customer" => $customer, "particulars" => $queryResult];
        return $data_to_be_sent;
    }
}