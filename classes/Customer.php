<?php

class Customer
{
    private $table = "customers";
    protected $di;
    private $database;
    private $columns = ['id', 'first_name', 'last_name', 'gst_no', 'phone_no', 'email_id', 'gender', 'created_at', 'updated_at'];
    private $validator;

    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function getValidator()
    {
        return $this->validator;
    }
    public function validateData($data, $id = "")
    {
        $this->validator = $this->di->get('validator');
        if($id == "")
        {
            $this->validator = $this->validator->check($data, [
                'first_name' => [
                    'required'=>true,
                    'minlength'=>3,
                    'maxlength'=>20,
                    'unique'=>$this->table
                    //here first_name cannot be unique bcoz there may be other person with same name
                    //but for my understanding in table i have kept unique so that i can easily managed
                ],
                'last_name' => [
                    'required'=>true,
                    'minlength'=>3,
                    'maxlength'=>20,
                    'unique'=>$this->table
                ],
                'gst_no' => [
                    'required'=>true,
                    'minlength'=>15,
                    'maxlength'=>15,
                    'unique'=>$this->table
                ],
                'phone_no' => [
                    'required'=>true,
                    'minlength'=>10,
                    'maxlength'=>10,
                    'unique'=>$this->table
                ],
                'email_id' => [
                    'required'=>true,
                    'minlength'=>10,
                    'unique'=>$this->table
                ],
                'gender' => [
                    'required'=>true,
                    'minlength'=>1
                ],
                'created_at' => [
                    'required'=>true,
                ],
                'updated_at' => [
                    'required'=>true,
                ],
            ]);
        }else
        {
            // Util::dd("Inside else update");
            //If we are inside this else so here we are sure that we got the columnName as well as the id that we are updating
            $this->validator = $this->validator->check($data, [
                'first_name' => [
                    'required'=>true,
                    'minlength'=>3,
                    'maxlength'=>20,
                    'uniqueExceptCurrentId'=>$this->table . "." . $id
                    //here first_name cannot be unique bcoz there may be other person with same name
                    //but for my understanding in table i have kept unique so that i can easily managed
                ],
                'last_name' => [
                    'required'=>true,
                    'minlength'=>3,
                    'maxlength'=>20,
                    // 'unique'=>$this->table
                    'uniqueExceptCurrentId'=>$this->table . "." . $id
                ],
                'gst_no' => [
                    'required'=>true,
                    'minlength'=>15,
                    'maxlength'=>15,
                    // 'unique'=>$this->table
                    'uniqueExceptCurrentId'=>$this->table . "." . $id
                ],
                'phone_no' => [
                    'required'=>true,
                    'minlength'=>10,
                    'maxlength'=>10,
                    // 'unique'=>$this->table
                    'uniqueExceptCurrentId'=>$this->table . "." . $id
                ],
                'email_id' => [
                    'required'=>true,
                    'minlength'=>10,
                    // 'unique'=>$this->table
                    'uniqueExceptCurrentId'=>$this->table . "." . $id
                ],
                'gender' => [
                    'required'=>true,
                    'minlength'=>1
                ],
                'created_at' => [
                    'required'=>true,
                ],
                'updated_at' => [
                    'required'=>true,
                ],
            ]);
        }

    }
    public function addCustomer($data)
    {
        //VALIDATE DATA
        $this->validateData($data);
        // Util::dd($this->validator->errors());

        //INSERT DATA IN DATABASE
        if(!$this->validator->fails())
        {
            try
            {
                $this->database->beginTransaction();

                $data_to_be_inserted = ['first_name'=>$data['first_name']];
                $data_to_be_inserted['last_name'] = $data['last_name'];
                $data_to_be_inserted['gst_no'] = $data['gst_no'];
                $data_to_be_inserted['phone_no'] = $data['phone_no'];
                $data_to_be_inserted['email_id'] = $data['email_id'];
                $data_to_be_inserted['gender'] = $data['gender'];
                $data_to_be_inserted['created_at'] = $data['created_at'];
                $data_to_be_inserted['updated_at'] = $data['updated_at'];
                $data_to_be_inserted['deleted'] = 0;


                // Util::dd($data_to_be_inserted);
                $customer_id = $this->database->insert($this->table, $data_to_be_inserted);
                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch(Exception $e)
            {
                $this->database->rollBack();
                return ADD_ERROR;
            }
        }
        return VALIDATION_ERROR;
    }
    public function getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length)
    {
        //name error : search error
        $query = "SELECT * FROM {$this->table} WHERE deleted = 0";
        $totalRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";
        $filteredRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";
        $baseURL = BASEURL;
        if($search_parameter != null)
        {
            $query .= " AND name LIKE '%{$search_parameter}%'";
            $filteredRowCountQuery .= " AND name LIKE '%{$search_parameter}%'";
        }

        if($order_by != null)
        {
            $query .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
        }
        else
        {
            $query .= " ORDER BY {$this->columns[0]} ASC";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[0]} ASC";
        }

        if($length != -1)
        {
            $query .= " LIMIT {$start}, {$length}";
        }

        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;
        
        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->total_count : 0;

        // Util::dd($query);
        $fetchedData = $this->database->raw($query);
        // Util::dd($fetchedData);
        $data = [];
        $numRows = is_array($fetchedData) ? count($fetchedData) : 0;
        for($i = 0; $i<$numRows; $i++)
        {
            $subArray = [];
            $subArray[] = $start+$i+1;
            $subArray[] = $fetchedData[$i]->first_name;
            $subArray[] = $fetchedData[$i]->last_name;
            $subArray[] = $fetchedData[$i]->gst_no;
            $subArray[] = $fetchedData[$i]->phone_no;
            $subArray[] = $fetchedData[$i]->email_id;
            $subArray[] = $fetchedData[$i]->gender;
            $subArray[] = $fetchedData[$i]->created_at;
            $subArray[] = $fetchedData[$i]->updated_at;
            // <input type="hidden" name="customer_id" value="{$fetchedData[$i]->id}" />
            // <form action="{$baseURL}helper/routing.php" method="POST">
            // Utill::dd($fetchedData[$i]->id);
            $subArray[] = <<<BUTTONS
<button class="btn btn-primary btn-sm edit" name="edit_customer"  data-id='{$fetchedData[$i]->id}'><i class="fas fa-pencil-alt"></i></button>  <button class="btn btn-outline-danger btn-sm" data-id='{$fetchedData[$i]->id}'><i class="fas fa-trash-alt"></i></button>
BUTTONS;
// </form>  
            $data[] = $subArray;
        }
        $output = array(
            'draw'=>$draw,
            'recordsTotal'=>$numberOfTotalRows,
            'recordsFiltered'=>$numberOfFilteredRows,
            'data'=>$data
        );
        echo json_encode($output);

    }
    public function getCustomerById($id, $fetchMode = PDO::FETCH_OBJ)
    {
        $query = "SELECT * FROM {$this->table} WHERE id = {$id}";
        $result= $this->database->raw($query, $fetchMode);
        return $result;
    }
    public function getCustomerByEmail($email)
    {
        $sql = "SELECT * FROM customers WHERE customers.email_id = '{$email}'";
        $res = $this->database->raw($sql);
        return $res;
    }
    public function update($data, $id)
    {
        $this->validateData($data, $id);
        // Util::dd($id);
        // Util::dd($data);
        if(!$this->validator->fails())
        {
            try
            {
                $this->database->beginTransaction();
                $data_to_be_updated = ['first_name' => $data['first_name']];
                $data_to_be_updated['last_name'] = $data['last_name'];
                $data_to_be_updated['gst_no'] = $data['gst_no'];
                $data_to_be_updated['phone_no'] = $data['phone_no'];
                $data_to_be_updated['email_id'] = $data['email_id'];
                $data_to_be_updated['gender'] = $data['gender'];
                $data_to_be_updated['updated_at'] = $data['updated_at'];
                // Util::dd($data_to_be_updated);
                $this->database->update($this->table, $data_to_be_updated, "id = {$id}");
                $this->database->commit();
                return UPDATE_SUCCESS;
            }catch(Exception $e)
            {
                $this->database->rollBack();
                return UPDATE_ERROR;
            }
        }
        else
        {
            return VALIDATION_ERROR;
        }
    }
    public function delete( $id)
    {
        try
        {
            $this->database->beginTransaction();
            $this->database->delete($this->table, "id = {$id}");
            $this->database->commit();
            return DELETE_SUCCESS;
        }catch(Exception $e)
        {
            $this->database->rollBack();
            return DELETE_ERROR;
        }
    }
}
